FROM ubuntu:18.04
WORKDIR /app
COPY setup-build-env.sh .
RUN bash setup-build-env.sh
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
COPY . .

