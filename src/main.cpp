#include <Arduino.h>
#include <FastLED.h>

#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include <MD5Builder.h>

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include <ESP8266mDNS.h>

MD5Builder _md5;

String md5(String str)
{
  _md5.begin();
  _md5.add(String(str));
  _md5.calculate();
  return _md5.toString();
}

#define NUM_LEDS 60
#define DATA_PIN D4
#define DEFAULT_COLOR 0xFF9329
CRGB leds[NUM_LEDS];


const char *mqttServer = "lightpi" ; //"192.168.178.178";
const int mqttPort = 1883;
unsigned long timeMillis;



WiFiClient espClient;
PubSubClient client(espClient);

boolean led_state(){
  boolean state = false;
  for(int i = 0; i < NUM_LEDS; i++){
    state = state || ((leds[i].r + leds[i].g + leds[i].b) != 0);
  }
  return state;
}

void dim_lights(unsigned int colorcode){
  Serial.printf("Got %u\n", colorcode);
  
  for(int i = 0; i < NUM_LEDS; i++){
    leds[i].maximizeBrightness();
    leds[i].nscale8_video( colorcode % 256 );
  }
  FastLED.show();
  Serial.printf("Faded by %u\n", colorcode % 256);
}

void led_effect()
{
  Serial.println("Start Light Effect");
  int tNUM_LEDS = NUM_LEDS;
  for (int j = 0; j < NUM_LEDS; j++)
  {
    for (int dot = 0; dot < tNUM_LEDS; dot++)
    {
      leds[dot] = CRGB::Lavender;
      FastLED.show();
      // clear this led for the next time around the loop
      leds[dot] = CRGB::Black;
      delay(120);
      ArduinoOTA.handle();
    }
    leds[NUM_LEDS - j - 1] = CRGB::Lavender;
    FastLED.show();
    tNUM_LEDS--;
  }
  Serial.println("END Light Effect");
}
void fadeout(unsigned int colorcode)
{
  int brightness = 0;
  for (int i = 0; i < NUM_LEDS; i++)
  {
    // int brightness = (int) 255 -  (i/ (double) NUM_LEDS) *  255.0;
    
    if (i <= NUM_LEDS / 2){
      brightness = brightness + ( (int) (255/NUM_LEDS));
    }else{
      brightness = brightness - ( (int) (255/NUM_LEDS));
    }
    leds[i].fadeLightBy(255-brightness);
    Serial.printf("Setting Brightness %d to %d\n", i, brightness);
    //leds[i].fadeLightBy(brightness);
    // if( i < 10) {leds[ 9 - i].fadeLightBy(brightness);
    // if( i > 20) leds[20 - i].fadeLightBy(brightness);
  }
  
}
void set_lights(unsigned int colorcode)
{
  //led_effect();
  Serial.printf("Setting Lights %x\n", colorcode);
  fill_solid(leds, NUM_LEDS, CRGB(colorcode));
  //blur1d(leds, NUM_LEDS, 25);
  //FastLED.show();
  //fadeout(colorcode);
  //fadeUsingColor(leds, NUM_LEDS, CRGB(colorcode));
  FastLED.show();
}

void mqtt_reconnect()
{
  while (!client.connected())
  {
    Serial.println("Connecting to MQTT...");

    if (client.connect(WiFi.macAddress().c_str()))
    {
      delay(500);
      Serial.println("connected");
      client.subscribe(String("esp/" + md5(WiFi.macAddress()) + "/lights/#").c_str());
    }
    else
    {

      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
    }
  }
}
void callback(char *topic, byte *payload, unsigned int length)
{
  Serial.println(topic);
  String payload_buff;
  for (unsigned int i = 0; i < length; i++)
  {
    payload_buff = payload_buff + String((char)payload[i]);
  }
  Serial.println(payload_buff); // Print out messages.
  if (String(topic).endsWith("/lights"))
  {
    set_lights(strtoul(payload_buff.c_str(), NULL, 16));
  }
  if (String(topic).endsWith("/lights/dim"))
  {
    dim_lights(strtoul(payload_buff.c_str(), NULL, 16));
  }
  client.publish(String("esp/" + md5(WiFi.macAddress()) + "/state").c_str(), String(led_state()).c_str() );
}

void setup()
{
  Serial.begin(9600);
  timeMillis = millis();
  Serial.println("Booting");
  
 
  WiFiManager wifiManager;
  wifiManager.autoConnect("AutoConnectAP");
 
  client.setServer(mqttServer, mqttPort);
  // Port defaults to 8266
  ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname("esp8266");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
    {
      type = "sketch";
    }
    else
    { // U_FS
      type = "filesystem";
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR)
    {
      Serial.println("Auth Failed");
    }
    else if (error == OTA_BEGIN_ERROR)
    {
      Serial.println("Begin Failed");
    }
    else if (error == OTA_CONNECT_ERROR)
    {
      Serial.println("Connect Failed");
    }
    else if (error == OTA_RECEIVE_ERROR)
    {
      Serial.println("Receive Failed");
    }
    else if (error == OTA_END_ERROR)
    {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  if (!MDNS.begin("esp8266")) {             // Start the mDNS responder for esp8266.local
    Serial.println("Error setting up MDNS responder!");
  }
  Serial.println("mDNS responder started");
  client.setCallback(callback);
  mqtt_reconnect();
  client.publish(String("esp/" + md5(WiFi.macAddress()) + "/hello").c_str(), "HELLO"); //Topic name
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  client.publish(String("esp/" + md5(WiFi.macAddress()) + "/state").c_str(), String(led_state()).c_str() ); //Topic name
  set_lights(DEFAULT_COLOR);
  pinMode(LED_BUILTIN, OUTPUT); // LED als Output definieren
  digitalWrite(LED_BUILTIN, LOW); // Ausschalten
}

void loop()
{
  
  mqtt_reconnect();
  client.loop();
  if (millis() - timeMillis > 5000){
    client.publish(String("esp/" + md5(WiFi.macAddress()) + "/state").c_str(), String(led_state()).c_str() );
    timeMillis = millis();
  }
  ArduinoOTA.handle(); 
  
}
